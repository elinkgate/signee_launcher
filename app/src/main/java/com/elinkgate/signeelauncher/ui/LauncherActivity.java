package com.elinkgate.signeelauncher.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.elinkgate.signeelauncher.R;
import com.elinkgate.signeelauncher.adapters.AppAdapters;
import com.elinkgate.signeelauncher.listeners.OnAppClickedListener;
import com.elinkgate.signeelauncher.models.ActionEntry;
import com.elinkgate.signeelauncher.models.ActionEntryFactory;
import com.elinkgate.signeelauncher.models.AppModel;
import com.elinkgate.signeelauncher.receivers.BaseBroadcastReceiver;
import com.elinkgate.signeelauncher.receivers.ClockBroadcastReceiver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LauncherActivity extends FullScreenActivity implements OnAppClickedListener {

    public static final int APP_LIST_SPAN_COUNT = 5;
    private TextView clockTv;
    private ImageButton settingBtn;
    private BaseBroadcastReceiver clockBroadcastReceiver;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm\nEEEE, MMMM dd, YYYY", Locale.ENGLISH);
    private static final ExecutorService executor = Executors.newFixedThreadPool(4);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        clockTv = findViewById(R.id.clock_tv);
        clockTv.setText(dateFormat.format(new Date()));

        settingBtn = findViewById(R.id.settings_icon);

        clockBroadcastReceiver = new ClockBroadcastReceiver((Context context, Intent intent) -> {
            clockTv.setText(dateFormat.format(new Date()));
        });

        RecyclerView recyclerView = findViewById(R.id.app_list);
        recyclerView.setLayoutManager(new GridLayoutManager(this, APP_LIST_SPAN_COUNT));

        AppAdapters adapter = new AppAdapters(loadApps());
        adapter.setAppClickedListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAppClicked(AppModel model) {
        if (model.isActionEntry()) {
            ((ActionEntry) model).perform();
        } else {
            Intent intent = getPackageManager().getLaunchIntentForPackage(model.getPackageName().toString());
            startActivity(intent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        clockBroadcastReceiver.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        clockBroadcastReceiver.unregister(this);
    }

    public @NonNull
    List<AppModel> loadApps() {
        final PackageManager packageManager = getPackageManager();
        List<AppModel> appList = new ArrayList<>();
        initActionEntry(appList);
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> availableActivities = packageManager.queryIntentActivities(intent, 0);
        AppModel appModel;
        for (final ResolveInfo resolveInfo : availableActivities) {
            appModel = new AppModel();
            appModel.setLabel(resolveInfo.loadLabel(packageManager));
            appModel.setPackageName(resolveInfo.activityInfo.packageName);
            appModel.setLoadIconDelegate((imageView, model) -> {
                loadAppIconAsync(resolveInfo, packageManager, imageView);
            });
            appList.add(appModel);
        }
        return appList;
    }

    private void initActionEntry(@NonNull List<AppModel> appList) {
        AppModel actionEntry = ActionEntryFactory.getStartDemoAction(this);
        actionEntry.setLoadIconDelegate((imageView, model) -> {
            loadActionEntryIconAsync(((ActionEntry) model).getIconResource(), imageView);
        });
        appList.add(actionEntry);
    }

    private void loadAppIconAsync(ResolveInfo resolveInfo, PackageManager packageManager, @Nullable ImageView imageView) {
        if (imageView == null) return;
        executor.execute(() -> {
            Drawable icon = resolveInfo.loadIcon(packageManager);
            this.runOnUiThread(() -> imageView.setImageDrawable(icon));
        });
    }

    private void loadActionEntryIconAsync(int iconResource, @Nullable ImageView imageView) {
        if (imageView == null) return;
        executor.execute(() -> {
            Drawable icon = this.getResources().getDrawable(iconResource);
            this.runOnUiThread(() -> imageView.setImageDrawable(icon));
        });
    }
}

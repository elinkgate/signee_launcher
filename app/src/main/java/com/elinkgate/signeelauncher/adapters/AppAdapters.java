package com.elinkgate.signeelauncher.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.elinkgate.signeelauncher.R;
import com.elinkgate.signeelauncher.listeners.OnAppClickedListener;
import com.elinkgate.signeelauncher.models.AppModel;

import java.util.List;

public class AppAdapters extends RecyclerView.Adapter<AppAdapters.ViewHolder> {

    private List<AppModel> appList;
    private @Nullable OnAppClickedListener appClickedListener = null;

    public AppAdapters(List<AppModel> appList) {
        this.appList = appList;
    }

    public void setAppClickedListener(@Nullable OnAppClickedListener appClickedListener) {
        this.appClickedListener = appClickedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(appList.get(position), appClickedListener);
    }

    @Override
    public int getItemCount() {
        return appList.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {

        private TextView labelTv;
        private ImageView iconImg;
        private View appItem;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            labelTv = itemView.findViewById(R.id.app_label);
            iconImg = itemView.findViewById(R.id.app_icon);
            appItem = itemView.findViewById(R.id.app_item);

            appItem.setFocusable(true);
            appItem.setFocusableInTouchMode(true);
        }

        public void bind(final AppModel appModel, final OnAppClickedListener listener) {

            labelTv.setText(appModel.getLabel());
            if (appModel.getIconDelegate() != null) {
                appModel.getIconDelegate().loadIconForImageView(iconImg, appModel);
            }

            appItem.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onAppClicked(appModel);
                }
            });
        }
    }
}

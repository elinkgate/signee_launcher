package com.elinkgate.signeelauncher.listeners;

import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.elinkgate.signeelauncher.models.AppModel;

public interface LoadIconDelegate {
    void loadIconForImageView(@Nullable ImageView imageView, AppModel appModel);
}

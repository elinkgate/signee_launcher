package com.elinkgate.signeelauncher.listeners;

import android.content.Context;
import android.content.Intent;

public interface OnReceivedListener {
    void onReceived(Context context, Intent intent);
}

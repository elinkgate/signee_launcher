package com.elinkgate.signeelauncher.listeners;

import com.elinkgate.signeelauncher.models.AppModel;

public interface OnAppClickedListener {
    void onAppClicked(AppModel appModel);
}

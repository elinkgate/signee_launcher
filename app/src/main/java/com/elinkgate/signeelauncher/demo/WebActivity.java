/**
 * Signee Sample App
 * Author: Huynh Duc Hau
 * Copyright ElinkGate JSC 2019
 */
package com.elinkgate.signeelauncher.demo;

import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.elinkgate.signeelauncher.ui.FullScreenActivity;
import com.elinkgate.signeelauncher.R;

public class WebActivity extends FullScreenActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        WebView webView = (WebView) findViewById(R.id.webview);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                             WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        WebViewClientImpl webViewClient = new WebViewClientImpl();
        webView.setWebViewClient(webViewClient);
        Bundle extras = getIntent().getExtras();
        webView.loadUrl(extras.getString("URL"));
    }
}

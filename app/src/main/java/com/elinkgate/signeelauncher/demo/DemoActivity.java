package com.elinkgate.signeelauncher.demo;

import android.app.ActivityOptions;
import android.app.admin.DevicePolicyManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.elinkgate.signeelauncher.ui.FullScreenActivity;
import com.elinkgate.signeelauncher.R;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

public class DemoActivity extends FullScreenActivity {

    public final String HDMI_DISPLAY = "HDMI";
    public final String BUILTIN_DISPLAY = "Built-in";

    private static final String TAG = "MainActivity";
    /* Bluetooth API */
    private BluetoothManager mBluetoothManager;
    private BluetoothGattServer mBluetoothGattServer;
    private BluetoothLeAdvertiser mBluetoothLeAdvertiser;
    /* Collection of notification subscribers */
    private Set<BluetoothDevice> mRegisteredDevices = new HashSet<>();

    /* Wifi API */
    private WifiManager mWifiManager;
    private String wifiSSid;
    private String wifiPsk;

    private ComponentName mAdminComponentName;
    private DevicePolicyManager mDevicePolicyManager;

    /**
     * Check if display id is valid or not
     * @param displayId
     * @return true: valid
     */
    private boolean Display_Valid(int displayId) {
        /* Check if given display id is valid */
        if(displayId >= ((DisplayManager) getSystemService(DISPLAY_SERVICE)).getDisplays().length)
            return false;

        return true;
    }
    /**
     * Start an Activity on a specified display
     * @param activityClass Activity class, eg: MainActivity.class
     * @param displayId Id of display: 0 for built-in, 1 for hdmi
     * @return true: success
     */
    private boolean LaunchOnDisplay(final Class<? extends AppCompatActivity>  activityClass,
                                    int displayId,
                                    Bundle param) {
        if(!Display_Valid(displayId))
            return false;
        ActivityOptions options = ActivityOptions.makeBasic();
        options.setLaunchDisplayId(displayId);
        Intent intent = new Intent(this, activityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(param);
        startActivity(intent, options.toBundle());
        return true;
    }

    /**
     * Start system viewer on specified display
     * @param mediaPath path to local media file
     * @param displayId Id of display: 0 for built-in, 1 for hdmi
     * @return
     */
    private boolean LaunchViewer(String mediaPath, int displayId) {
        if(!Display_Valid(displayId))
            return false;
        ActivityOptions options = ActivityOptions.makeBasic();
        options.setLaunchDisplayId(displayId);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mediaPath));
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent, options.toBundle());
        return true;
    }

    /**
     * Start Gallery viewer on specified display
     * @param mediaPath path to local media file
     * @param displayId Id of display: 0 for built-in, 1 for hdmi
     * @return
     */
    private boolean LaunchGalleryViewer(String mediaPath, int displayId) {
        if(!Display_Valid(displayId))
            return false;
        File file = new File(mediaPath);
        if(!file.exists()) {
            return false;
        }
        ActivityOptions options = ActivityOptions.makeBasic();
        options.setLaunchDisplayId(displayId);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("file://" + mediaPath));
        intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName("com.android.gallery3d", "com.android.gallery3d.app.MovieActivity");
        startActivity(intent, options.toBundle());
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        mAdminComponentName  = new MyDeviceAdminReceiver().getComponentName(this);
        mDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

        // Start demo view on second display
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Fill content on Display 1 */
                Bundle bundle = new Bundle();
                bundle.putString("URL", "file:///android_asset/demo.html");
                LaunchOnDisplay(WebActivity.class, 0, bundle);

                /* Fill content on Display 0 */
                Bundle bundle2 = new Bundle();
                bundle2.putString("URL", "https://player.vimeo.com/video/43217960?autoplay=1&loop=1&title=0&byline=0&portrait=0");
                LaunchOnDisplay(WebActivity.class, 1, bundle2);
            }
        }, 3000);

        // Devices with a display should not go to sleep
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mBluetoothManager   = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mWifiManager        = (WifiManager) getSystemService(WIFI_SERVICE);
        mWifiManager.setWifiEnabled(true);
        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();
        // We can't continue without proper Bluetooth support
        if (!checkBluetoothSupport(bluetoothAdapter)) {
            finish();
        }
        bluetoothAdapter.setName("Signee Settings");//Careful Here
        // Register for system Bluetooth events
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBluetoothReceiver, filter);
        if (!bluetoothAdapter.isEnabled()) {
            Log.d(TAG, "Bluetooth is currently disabled...enabling");
            bluetoothAdapter.enable();
        } else {
            Log.d(TAG, "Bluetooth enabled...starting services");
            startAdvertising();
            startServer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();
        if (bluetoothAdapter.isEnabled()) {
            stopServer();
            stopAdvertising();
        }

        unregisterReceiver(mBluetoothReceiver);
    }


    /**
     * Verify the level of Bluetooth support provided by the hardware.
     * @param bluetoothAdapter System {@link BluetoothAdapter}.
     * @return true if Bluetooth is properly supported, false otherwise.
     */
    private boolean checkBluetoothSupport(BluetoothAdapter bluetoothAdapter) {

        if (bluetoothAdapter == null) {
            Log.w(TAG, "Bluetooth is not supported");
            return false;
        }

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.w(TAG, "Bluetooth LE is not supported");
            return false;
        }

        return true;
    }

    /**
     * Listens for Bluetooth adapter events to enable/disable
     * advertising and server functionality.
     */
    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);

            switch (state) {
                case BluetoothAdapter.STATE_ON:
                    startAdvertising();
                    startServer();
                    break;
                case BluetoothAdapter.STATE_OFF:
                    stopServer();
                    stopAdvertising();
                    break;
                default:
                    // Do nothing
            }
        }
    };

    /**
     * Begin advertising over Bluetooth that this device is connectable
     * and supports the Current Time Service.
     */
    private void startAdvertising() {
        BluetoothAdapter bluetoothAdapter = mBluetoothManager.getAdapter();
        mBluetoothLeAdvertiser = bluetoothAdapter.getBluetoothLeAdvertiser();
        if (mBluetoothLeAdvertiser == null) {
            Log.w(TAG, "Failed to create advertiser");
            return;
        }

        AdvertiseSettings settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build();

        AdvertiseData data = new AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(false)
                .build();

        mBluetoothLeAdvertiser
                .startAdvertising(settings, data, mAdvertiseCallback);
    }

    /**
     * Stop Bluetooth advertisements.
     */
    private void stopAdvertising() {
        if (mBluetoothLeAdvertiser == null) return;
        mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);
    }

    /**
     * Initialize the GATT server instance with the services/characteristics
     * from the Time Profile.
     */
    private void startServer() {
        mBluetoothGattServer = mBluetoothManager.openGattServer(this, mGattServerCallback);
        if (mBluetoothGattServer == null) {
            Log.w(TAG, "Unable to create GATT server");
            return;
        }

        mBluetoothGattServer.addService(SigneeProfile.createNetworkService());
    }

    /**
     * Shut down the GATT server.
     */
    private void stopServer() {
        if (mBluetoothGattServer == null) return;

        mBluetoothGattServer.close();
    }

    /**
     * Callback to receive information about the advertisement process.
     */
    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            Log.i(TAG, "LE Advertise Started.");
        }

        @Override
        public void onStartFailure(int errorCode) {
            Log.w(TAG, "LE Advertise Failed: "+errorCode);
        }
    };

    private void wifiConnect(String ssid, String psk) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = String.format("\"%s\"", ssid);
        wifiConfig.preSharedKey = String.format("\"%s\"", psk);

        int netId = mWifiManager.addNetwork(wifiConfig);
        mWifiManager.disconnect();
        mWifiManager.enableNetwork(netId, true);
        mWifiManager.reconnect();
    }

    /**
     * Callback to handle incoming requests to the GATT server.
     * All read/write requests for characteristics and descriptors are handled here.
     */
    private BluetoothGattServerCallback mGattServerCallback = new BluetoothGattServerCallback() {

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "BluetoothDevice CONNECTED: " + device);
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, "BluetoothDevice DISCONNECTED: " + device);
                //Remove device from any active subscriptions
                mRegisteredDevices.remove(device);
            }
        }

        @Override
        public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset,
                                                BluetoothGattCharacteristic characteristic) {
            if (SigneeProfile.WIFI_SSID.equals(characteristic.getUuid())) {
                Log.i(TAG, "Read Wifi SSID: " + mWifiManager.getConnectionInfo().getSSID());
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_SUCCESS,
                        0,
                        mWifiManager.getConnectionInfo().getSSID().getBytes());
            } else if (SigneeProfile.WIFI_PSK.equals(characteristic.getUuid())) {
                Log.i(TAG, "Read Wifi PSK");
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_SUCCESS,
                        0, "********".getBytes());
            } else {
                // Invalid characteristic
                Log.w(TAG, "Invalid Characteristic Read: " + characteristic.getUuid());
                mBluetoothGattServer.sendResponse(device,
                        requestId,
                        BluetoothGatt.GATT_FAILURE,
                        0,
                        null);
            }
        }

        @Override
        public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId,
                                                 BluetoothGattCharacteristic characteristic,
                                                 boolean preparedWrite,
                                                 boolean responseNeeded,
                                                 int offset,
                                                 byte[] value) {
            if (SigneeProfile.WIFI_SSID.equals(characteristic.getUuid())) {
                wifiSSid = new String(value, StandardCharsets.UTF_8);
                Log.i(TAG, "Write Wifi SSID: " + wifiSSid);
                if(responseNeeded)
                    mBluetoothGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_SUCCESS,
                            0,
                            null);
            } else if (SigneeProfile.WIFI_PSK.equals(characteristic.getUuid())) {
                wifiPsk = new String(value, StandardCharsets.UTF_8);
                Log.i(TAG, "Write Wifi PSK: " + wifiPsk);
                if(responseNeeded)
                    mBluetoothGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_SUCCESS,
                            0,
                            null);
                /* Start connecting to new SSID */
                wifiConnect(wifiSSid,wifiPsk);
            } else {
                // Invalid characteristic
                Log.w(TAG, "Invalid Characteristic Write: " + characteristic.getUuid());
                if(responseNeeded)
                    mBluetoothGattServer.sendResponse(device,
                            requestId,
                            BluetoothGatt.GATT_FAILURE,
                            0,
                            null);
            }
        }
    };
}

/**
 * Signee Sample App
 * Author: Huynh Duc Hau
 * Copyright ElinkGate JSC 2019
 */
package com.elinkgate.signeelauncher.demo;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import java.util.UUID;

public class SigneeProfile {
    public static UUID NETWORK_SERVICE = UUID.fromString("05af8deb-b5b7-427a-8d73-b80026289a45");
    public static UUID WIFI_SSID = UUID.fromString("5530460e-4d3f-46a7-8fd6-0e44c0135941");
    public static UUID WIFI_PSK = UUID.fromString("cc96b1af-e50a-4664-9d66-102bc15c6abb");

//    public static UUID STATUS_SERVICE = UUID.fromString("00000100-b5b7-427a-8d73-b80026289a45");
    /**
     * Return a configured {@link BluetoothGattService} instance for the
     * Current Time Service.
     */
    public static BluetoothGattService createNetworkService() {
        BluetoothGattService service = new BluetoothGattService(NETWORK_SERVICE,
                BluetoothGattService.SERVICE_TYPE_PRIMARY);

        BluetoothGattCharacteristic wifiSsid = new BluetoothGattCharacteristic(WIFI_SSID,
                //Read-only characteristic, supports notifications
                BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);

        BluetoothGattCharacteristic wifiPsk = new BluetoothGattCharacteristic(WIFI_PSK,
                BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);

        service.addCharacteristic(wifiSsid);
        service.addCharacteristic(wifiPsk);

        return service;
    }
}

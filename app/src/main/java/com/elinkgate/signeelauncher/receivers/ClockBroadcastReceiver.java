package com.elinkgate.signeelauncher.receivers;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.annotation.Nullable;

import com.elinkgate.signeelauncher.listeners.OnReceivedListener;

public class ClockBroadcastReceiver extends BaseBroadcastReceiver {

    public ClockBroadcastReceiver(@Nullable OnReceivedListener onReceivedListener) {
        super(onReceivedListener);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (onReceivedListener != null && intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
            onReceivedListener.onReceived(context, intent);
        }
    }

    @Override
    public void register(Context context) {
        context.registerReceiver(this, new IntentFilter(Intent.ACTION_TIME_TICK));
    }
}

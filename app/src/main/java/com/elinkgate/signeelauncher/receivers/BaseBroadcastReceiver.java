package com.elinkgate.signeelauncher.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.elinkgate.signeelauncher.listeners.OnReceivedListener;

public abstract class BaseBroadcastReceiver extends BroadcastReceiver {

    @Nullable
    protected OnReceivedListener onReceivedListener;

    public BaseBroadcastReceiver(@Nullable OnReceivedListener onReceivedListener) {
        this.onReceivedListener = onReceivedListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (onReceivedListener != null) {
            onReceivedListener.onReceived(context, intent);
        }
    }

    abstract public void register(Context context);

    public void unregister(Context context) {
        context.unregisterReceiver(this);
    }
}

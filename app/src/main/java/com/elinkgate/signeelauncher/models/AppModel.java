package com.elinkgate.signeelauncher.models;

import com.elinkgate.signeelauncher.listeners.LoadIconDelegate;

public class AppModel {

    private CharSequence label;
    private CharSequence packageName;
    private LoadIconDelegate iconDelegate;
    private boolean hidden;

    public void setLoadIconDelegate(LoadIconDelegate iconDelegate) {
        this.iconDelegate = iconDelegate;
    }

    public void setLabel(CharSequence label) {
        this.label = label;
    }

    public void setPackageName(CharSequence packageName) {
        this.packageName = packageName;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public CharSequence getLabel() {
        return label;
    }

    public CharSequence getPackageName() {
        return packageName;
    }

    public boolean isHidden() {
        return hidden;
    }

    public LoadIconDelegate getIconDelegate() {
        return iconDelegate;
    }

    public boolean isActionEntry() {
        return false;
    }
}

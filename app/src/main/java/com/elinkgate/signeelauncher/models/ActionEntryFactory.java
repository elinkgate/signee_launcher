package com.elinkgate.signeelauncher.models;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import com.elinkgate.signeelauncher.R;
import com.elinkgate.signeelauncher.demo.DemoActivity;

public class ActionEntryFactory {

    private ActionEntryFactory() {}

    public static ActionEntry getStartDemoAction(@NonNull final Context context) {
        return new ActionEntry(() -> {
            Intent intent = new Intent(context, DemoActivity.class);
            context.startActivity(intent);
        }, "Start demo", "com.elinkgate.startdemo", R.drawable.ic_ondemand_video_black_24dp);
    }
}

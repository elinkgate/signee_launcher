package com.elinkgate.signeelauncher.models;

import androidx.annotation.Nullable;

public class ActionEntry extends AppModel {

    private @Nullable
    Runnable action;
    private int iconResource;

    public ActionEntry(@Nullable Runnable action, String label, String packageName, int iconResource) {
        this.action = action;
        this.setPackageName(packageName);
        this.setLabel(label);
        this.iconResource = iconResource;
    }

    public void setAction(@Nullable Runnable action) {
        this.action = action;
    }

    public void perform() {
        if (action != null) {
            this.action.run();
        }
    }

    @Nullable
    public Runnable getAction() {
        return action;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    @Override
    public boolean isActionEntry() {
        return true;
    }
}
